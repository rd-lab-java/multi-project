import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomUtilsTest {

    @Test
    @DisplayName("Simple negative numbers check")
    public void testNegativeCheckA() {
        assertFalse(CustomUtils.isAllPositiveNumbers("10", "-1"));
    }

    @Test
    @DisplayName("Simple negative numbers check")
    public void testNegativeCheckB() {
        assertFalse(CustomUtils.isAllPositiveNumbers("-100", "0"));
    }

    @Test
    @DisplayName("Simple positive numbers check")
    public void testPositiveCheckA() {
        assertTrue(CustomUtils.isAllPositiveNumbers("1", "1"));
    }

    @Test
    @DisplayName("Simple positive numbers check")
    public void testPositiveCheckB() {
        assertTrue(CustomUtils.isAllPositiveNumbers("100000", "999999999999"));
    }

    @Test
    @DisplayName("Simple invalid numbers check")
    public void testInvalidCheckA() {
        assertFalse(CustomUtils.isAllPositiveNumbers("0aa", "11"));
    }

    @Test
    @DisplayName("Simple invalid numbers check")
    public void testInvalidCheckB() {
        assertFalse(CustomUtils.isAllPositiveNumbers("asvaa", "dllld"));
    }
}