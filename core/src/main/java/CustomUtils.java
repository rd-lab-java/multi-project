/**
 * <p> Provides extra functionality for NumberUtils of Apache Common Lang Api.</p>
 */
public class CustomUtils {

    /**
     * <p>Checks array of numbers whether
     *     {@code String} contains only positive numbers.</p>
     *
     * <p>{@code null} and empty/blank {@code String} will return
     * {@code false}.</p>
     *
     * @param str array of numbers, may be null
     * @return {@code true} if the string is a correctly formatted number
     */
    public static boolean isAllPositiveNumbers(String... str) {
        if(str == null || str.length == 0)
            return false;

        for(String s: str) {
            if(!Utils.isPositiveNumber(s))
                return false;
        }
        return true;
    }
}
